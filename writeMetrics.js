const { InfluxDB, Point } = require("@influxdata/influxdb-client");

const {
  INFLUXDB_ORG: influxOrg,
  INFLUXDB_BUCKET: influxBucket,
  INFLUXDB_ADMIN_TOKEN: influxToken,
  INFLUXDB_URL: influxUrl,
} = require("./env");

const client = new InfluxDB({ url: influxUrl, token: influxToken });
const writeApi = client.getWriteApi(influxOrg, influxBucket);


module.exports = async function writeMetrics({
    bodyScore,
    date,
    weight,
    BMI,
    bodyFat,
    water,
    visceralFat,
    protein,
    muscle,
    boneMass,
    basalMetabolism,
  }) {
    const point = new Point("scale")
      .timestamp(date)
      .intField('bodyScore', bodyScore)
      .floatField('weight', weight)
      .floatField('BMI', BMI)
      .floatField('bodyfat', bodyFat)
      .floatField('water', water)
      .floatField('boneMass', boneMass)
      .floatField('protein', protein)
      .floatField('muscle', muscle)
      .intField('visceralFat', visceralFat)
      .intField('basalMetabolism', basalMetabolism);

    writeApi.writePoint(point);
    await writeApi.flush();
  }
