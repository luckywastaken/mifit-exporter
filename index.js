process.env.NTBA_FIX_319 = 1; // Fix promise cancellation

const TelegramBot = require("node-telegram-bot-api");
const fs = require("fs");

const parseMetrics = require("./parseMetrics");
const parseJpg = require("./parseJpg");
const writeMetrics = require("./writeMetrics");
const {
  TG_TOKEN: token,
  MY_CHAT_ID: myChatId,
  IMAGES_DIR: imagesDir,
} = require("./env");

if (!fs.existsSync(imagesDir)) {
  fs.mkdirSync(imagesDir);
}

const bot = new TelegramBot(token, { polling: true });

bot.on("photo", async (msg) => {
  const chatId = msg.chat.id;

  if (chatId !== myChatId) {
    console.warn("Recieved msg from another chat");
    return;
  }

  try {
    const downloadedImage = await downloadBestPhoto(msg);
    const parsedText = await parseJpg(downloadedImage);
    const parsedMetrics = parseMetrics(parsedText);

    await writeMetrics(parsedMetrics);

    bot.sendMessage(chatId, "Parsed successfully!");
  } catch {
    bot.sendMessage(chatId, "Parsing error...");
  }
});

async function downloadBestPhoto(message) {
  try {
    const lastFile = message.photo[message.photo.length - 1];
    const fileId = lastFile.file_id;

    return bot.downloadFile(fileId, imagesDir);
  } catch (error) {
    console.error("Error downloading photo!", error);

    throw new Error("Error downloading photo!");
  }
}
