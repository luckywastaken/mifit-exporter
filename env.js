const { envsafe, str, num } = require("envsafe");

module.exports = envsafe({
  OCR_SPACE_API_KEY: str(),
  TG_TOKEN: str(),
  MY_CHAT_ID: num(),
  IMAGES_DIR: str({ default: "fit_imgs" }),
  INFLUXDB_ORG: str(),
  INFLUXDB_BUCKET: str(),
  INFLUXDB_ADMIN_TOKEN: str(),
  INFLUXDB_URL: str({ default: 'http://influx:8086' }),
});
