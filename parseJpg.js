const { ocrSpace } = require("ocr-space-api-wrapper");
const { OCR_SPACE_API_KEY: apiKey } = require("./env");

module.exports = async function parseJpg(path) {
  const result = await ocrSpace(path, { apiKey, OCREngine: 2 });

  const error = result.ParsedResults[0].ErrorMessage;

  if (error) {
    console.error(error);

    throw error;
  }

  return result.ParsedResults[0].ParsedText;
};
