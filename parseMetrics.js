function parseMetrics(text) {
  try {
    // Get fixed-lines metrics
    const lines = text.split("\n");
    const bodyScore = parseInt(lines[2]);
    const date = new Date(lines[4]);
    date.setFullYear(new Date().getFullYear());
    const weight = parseCommaFloat(lines[5]);

    // Get regexable metrics
    const BMI = findFloatByRegex(text, /BMI (\d+,\d+)/);
    const bodyFat = findFloatByRegex(text, /Body fat (\d+,\d+)/);
    const water = findFloatByRegex(text, /Water (\d+,\d+)/);
    const visceralFat = findFloatByRegex(text, /Visceral fat (\d+)/);
    const muscle = findFloatByRegex(text, /Muscle (\d+,\d+)/);
    const protein = findFloatByRegex(text, /Protein (\d+,\d+)/);
    const boneMass = findFloatByRegex(text, /Bone mass (\d+,\d+)/);
    const basalMetabolism = findBasalMetabolism(text);

    return {
      bodyScore,
      date,
      weight,
      BMI,
      bodyFat,
      water,
      visceralFat,
      muscle,
      protein,
      boneMass,
      basalMetabolism,
    };
  } catch (error) {
    console.error(error);

    throw error;
  }
}

function findBasalMetabolism(text) {
  const foundLine = text.match(/Basal metabolism (.*) kcal/);

  if (!foundLine) {
    throw new Error(`Found no lines for basal`);
  }

  return Number(foundLine[1].replace(" ", ""));
}

function findFloatByRegex(text, regex) {
  const foundLine = text.match(regex);

  if (!foundLine) {
    throw new Error(`Found no lines for regex: ${regex}`);
  }

  return parseCommaFloat(foundLine[1]);
}

function parseCommaFloat(string) {
  return parseFloat(string.replace(",", "."));
}

module.exports = parseMetrics;
